# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.7.0 (2021-09-27)


### Features

* add file .txt ([e2b76a7](https://bitbucket.org/mateustech/gitflow/commit/e2b76a7b6298da7d861323454f62163c5d23b918))
* add logs ([0fcb392](https://bitbucket.org/mateustech/gitflow/commit/0fcb392f65fc2131c392a31f60bc4e19db756aff))
* add new feature ([b314740](https://bitbucket.org/mateustech/gitflow/commit/b314740a7466a243ac6ca0996c0576057035d1a0))
* add new line ([60bb86e](https://bitbucket.org/mateustech/gitflow/commit/60bb86ea5789f14acf7a060b3612f113ed31cad7))
* add new line ([23bdf0a](https://bitbucket.org/mateustech/gitflow/commit/23bdf0a9998fda47a2104de352f68375d9b8dee9))
* add script get version ([493e36f](https://bitbucket.org/mateustech/gitflow/commit/493e36f02290d560763c8a47dd4be10b13fdfced))
* create file ramoparalelo ([fa129db](https://bitbucket.org/mateustech/gitflow/commit/fa129db5a27fadb766c3af4bbb76280e9a8452f1))
* edit file by ramo2 ([7b56e08](https://bitbucket.org/mateustech/gitflow/commit/7b56e0833bcba71a6da66bdc7d605355349fefdd))
* nova feature ([f399132](https://bitbucket.org/mateustech/gitflow/commit/f399132b50bf82d72d06b01f73c3d47c5318a979))
* nova feature ([6fb1401](https://bitbucket.org/mateustech/gitflow/commit/6fb14011e80498df7ff25a846c20ec1eb4f882a6))


### Bug Fixes

* agora vai da certo ([ccaf1d4](https://bitbucket.org/mateustech/gitflow/commit/ccaf1d4f81b5e3366665a3546148a23a1ccfb7e0))
* asd ([dc73633](https://bitbucket.org/mateustech/gitflow/commit/dc7363359aea5b3dfbb6d801a1c61256112c1a86))
* database bug fix ([178171d](https://bitbucket.org/mateustech/gitflow/commit/178171d9834359f771c90a2ca3597ff469022c77))
* jira[#325](https://bitbucket.org/mateustech/gitflow/issues/325) ([da3a599](https://bitbucket.org/mateustech/gitflow/commit/da3a59971775deae09bcf0f9e8d49d01bb54c1c4))
* modify and add line into developer ([4f048d5](https://bitbucket.org/mateustech/gitflow/commit/4f048d56ac59f1916d2565277cd360fcac4cc108))
* resolve bug ([fa02d4d](https://bitbucket.org/mateustech/gitflow/commit/fa02d4d7a7213ed6c474426f8bcc563f5f9ce520))
* resolve bug ([c431699](https://bitbucket.org/mateustech/gitflow/commit/c431699bfff1f3ea83412af9064e7a2ad8346b9e))
* tudo certo ([c41063b](https://bitbucket.org/mateustech/gitflow/commit/c41063b1fc1d482aff3b27d3c11d264ac0220dfe))

## 1.6.0 (2021-09-27)


### Features

* add file .txt ([e2b76a7](https://bitbucket.org/mateustech/gitflow/commit/e2b76a7b6298da7d861323454f62163c5d23b918))
* add logs ([0fcb392](https://bitbucket.org/mateustech/gitflow/commit/0fcb392f65fc2131c392a31f60bc4e19db756aff))
* add new feature ([b314740](https://bitbucket.org/mateustech/gitflow/commit/b314740a7466a243ac6ca0996c0576057035d1a0))
* add new line ([60bb86e](https://bitbucket.org/mateustech/gitflow/commit/60bb86ea5789f14acf7a060b3612f113ed31cad7))
* add new line ([23bdf0a](https://bitbucket.org/mateustech/gitflow/commit/23bdf0a9998fda47a2104de352f68375d9b8dee9))
* add script get version ([493e36f](https://bitbucket.org/mateustech/gitflow/commit/493e36f02290d560763c8a47dd4be10b13fdfced))
* create file ramoparalelo ([fa129db](https://bitbucket.org/mateustech/gitflow/commit/fa129db5a27fadb766c3af4bbb76280e9a8452f1))
* edit file by ramo2 ([7b56e08](https://bitbucket.org/mateustech/gitflow/commit/7b56e0833bcba71a6da66bdc7d605355349fefdd))
* nova feature ([f399132](https://bitbucket.org/mateustech/gitflow/commit/f399132b50bf82d72d06b01f73c3d47c5318a979))
* nova feature ([6fb1401](https://bitbucket.org/mateustech/gitflow/commit/6fb14011e80498df7ff25a846c20ec1eb4f882a6))


### Bug Fixes

* agora vai da certo ([ccaf1d4](https://bitbucket.org/mateustech/gitflow/commit/ccaf1d4f81b5e3366665a3546148a23a1ccfb7e0))
* asd ([dc73633](https://bitbucket.org/mateustech/gitflow/commit/dc7363359aea5b3dfbb6d801a1c61256112c1a86))
* jira[#325](https://bitbucket.org/mateustech/gitflow/issues/325) ([da3a599](https://bitbucket.org/mateustech/gitflow/commit/da3a59971775deae09bcf0f9e8d49d01bb54c1c4))
* modify and add line into developer ([4f048d5](https://bitbucket.org/mateustech/gitflow/commit/4f048d56ac59f1916d2565277cd360fcac4cc108))
* resolve bug ([fa02d4d](https://bitbucket.org/mateustech/gitflow/commit/fa02d4d7a7213ed6c474426f8bcc563f5f9ce520))
* resolve bug ([c431699](https://bitbucket.org/mateustech/gitflow/commit/c431699bfff1f3ea83412af9064e7a2ad8346b9e))
* tudo certo ([c41063b](https://bitbucket.org/mateustech/gitflow/commit/c41063b1fc1d482aff3b27d3c11d264ac0220dfe))

## 1.5.0 (2021-09-24)


### Features

* add file .txt ([e2b76a7](https://bitbucket.org/mateustech/gitflow/commit/e2b76a7b6298da7d861323454f62163c5d23b918))
* add new line ([60bb86e](https://bitbucket.org/mateustech/gitflow/commit/60bb86ea5789f14acf7a060b3612f113ed31cad7))
* add new line ([23bdf0a](https://bitbucket.org/mateustech/gitflow/commit/23bdf0a9998fda47a2104de352f68375d9b8dee9))
* add script get version ([493e36f](https://bitbucket.org/mateustech/gitflow/commit/493e36f02290d560763c8a47dd4be10b13fdfced))
* create file ramoparalelo ([fa129db](https://bitbucket.org/mateustech/gitflow/commit/fa129db5a27fadb766c3af4bbb76280e9a8452f1))
* edit file by ramo2 ([7b56e08](https://bitbucket.org/mateustech/gitflow/commit/7b56e0833bcba71a6da66bdc7d605355349fefdd))
* nova feature ([f399132](https://bitbucket.org/mateustech/gitflow/commit/f399132b50bf82d72d06b01f73c3d47c5318a979))
* nova feature ([6fb1401](https://bitbucket.org/mateustech/gitflow/commit/6fb14011e80498df7ff25a846c20ec1eb4f882a6))


### Bug Fixes

* modify and add line into developer ([4f048d5](https://bitbucket.org/mateustech/gitflow/commit/4f048d56ac59f1916d2565277cd360fcac4cc108))
* resolve bug ([fa02d4d](https://bitbucket.org/mateustech/gitflow/commit/fa02d4d7a7213ed6c474426f8bcc563f5f9ce520))
* resolve bug ([c431699](https://bitbucket.org/mateustech/gitflow/commit/c431699bfff1f3ea83412af9064e7a2ad8346b9e))

## 1.4.0 (2021-09-24)


### Features

* add file .txt ([e2b76a7](https://bitbucket.org/mateustech/gitflow/commit/e2b76a7b6298da7d861323454f62163c5d23b918))
* add new line ([60bb86e](https://bitbucket.org/mateustech/gitflow/commit/60bb86ea5789f14acf7a060b3612f113ed31cad7))
* add new line ([23bdf0a](https://bitbucket.org/mateustech/gitflow/commit/23bdf0a9998fda47a2104de352f68375d9b8dee9))
* add script get version ([493e36f](https://bitbucket.org/mateustech/gitflow/commit/493e36f02290d560763c8a47dd4be10b13fdfced))
* create file ramoparalelo ([fa129db](https://bitbucket.org/mateustech/gitflow/commit/fa129db5a27fadb766c3af4bbb76280e9a8452f1))
* edit file by ramo2 ([7b56e08](https://bitbucket.org/mateustech/gitflow/commit/7b56e0833bcba71a6da66bdc7d605355349fefdd))
* nova feature ([6fb1401](https://bitbucket.org/mateustech/gitflow/commit/6fb14011e80498df7ff25a846c20ec1eb4f882a6))


### Bug Fixes

* modify and add line into developer ([4f048d5](https://bitbucket.org/mateustech/gitflow/commit/4f048d56ac59f1916d2565277cd360fcac4cc108))
* resolve bug ([c431699](https://bitbucket.org/mateustech/gitflow/commit/c431699bfff1f3ea83412af9064e7a2ad8346b9e))

## 1.3.0 (2021-09-24)


### Features

* add file .txt ([e2b76a7](https://bitbucket.org/mateustech/gitflow/commit/e2b76a7b6298da7d861323454f62163c5d23b918))
* add new line ([60bb86e](https://bitbucket.org/mateustech/gitflow/commit/60bb86ea5789f14acf7a060b3612f113ed31cad7))
* add new line ([23bdf0a](https://bitbucket.org/mateustech/gitflow/commit/23bdf0a9998fda47a2104de352f68375d9b8dee9))
* add script get version ([493e36f](https://bitbucket.org/mateustech/gitflow/commit/493e36f02290d560763c8a47dd4be10b13fdfced))
* create file ramoparalelo ([fa129db](https://bitbucket.org/mateustech/gitflow/commit/fa129db5a27fadb766c3af4bbb76280e9a8452f1))
* edit file by ramo2 ([7b56e08](https://bitbucket.org/mateustech/gitflow/commit/7b56e0833bcba71a6da66bdc7d605355349fefdd))


### Bug Fixes

* modify and add line into developer ([4f048d5](https://bitbucket.org/mateustech/gitflow/commit/4f048d56ac59f1916d2565277cd360fcac4cc108))
* resolve bug ([c431699](https://bitbucket.org/mateustech/gitflow/commit/c431699bfff1f3ea83412af9064e7a2ad8346b9e))

## 1.2.0 (2021-09-24)


### Features

* add file .txt ([e2b76a7](https://bitbucket.org/mateustech/gitflow/commit/e2b76a7b6298da7d861323454f62163c5d23b918))
* add new line ([60bb86e](https://bitbucket.org/mateustech/gitflow/commit/60bb86ea5789f14acf7a060b3612f113ed31cad7))
* add new line ([23bdf0a](https://bitbucket.org/mateustech/gitflow/commit/23bdf0a9998fda47a2104de352f68375d9b8dee9))
* add script get version ([493e36f](https://bitbucket.org/mateustech/gitflow/commit/493e36f02290d560763c8a47dd4be10b13fdfced))
* create file ramoparalelo ([fa129db](https://bitbucket.org/mateustech/gitflow/commit/fa129db5a27fadb766c3af4bbb76280e9a8452f1))
* edit file by ramo2 ([7b56e08](https://bitbucket.org/mateustech/gitflow/commit/7b56e0833bcba71a6da66bdc7d605355349fefdd))


### Bug Fixes

* modify and add line into developer ([4f048d5](https://bitbucket.org/mateustech/gitflow/commit/4f048d56ac59f1916d2565277cd360fcac4cc108))
* resolve bug ([c431699](https://bitbucket.org/mateustech/gitflow/commit/c431699bfff1f3ea83412af9064e7a2ad8346b9e))

## 1.1.0 (2021-09-24)


### Features

* add file .txt ([e2b76a7](https://bitbucket.org/mateustech/gitflow/commit/e2b76a7b6298da7d861323454f62163c5d23b918))
* add new line ([60bb86e](https://bitbucket.org/mateustech/gitflow/commit/60bb86ea5789f14acf7a060b3612f113ed31cad7))
* add new line ([23bdf0a](https://bitbucket.org/mateustech/gitflow/commit/23bdf0a9998fda47a2104de352f68375d9b8dee9))
* add script get version ([493e36f](https://bitbucket.org/mateustech/gitflow/commit/493e36f02290d560763c8a47dd4be10b13fdfced))
* create file ramoparalelo ([fa129db](https://bitbucket.org/mateustech/gitflow/commit/fa129db5a27fadb766c3af4bbb76280e9a8452f1))
* edit file by ramo2 ([7b56e08](https://bitbucket.org/mateustech/gitflow/commit/7b56e0833bcba71a6da66bdc7d605355349fefdd))


### Bug Fixes

* modify and add line into developer ([4f048d5](https://bitbucket.org/mateustech/gitflow/commit/4f048d56ac59f1916d2565277cd360fcac4cc108))
* resolve bug ([c431699](https://bitbucket.org/mateustech/gitflow/commit/c431699bfff1f3ea83412af9064e7a2ad8346b9e))
